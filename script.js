class User {
  constructor(name, email) {
    this.name = name;
    this.email = email;
    this.points = [];
    this.currentPoint = 0;

    User.arrayOfUsers.push(this);
  }

  addPoint(point) {
    this.points.push(point);
    this.currentPoint = point;
  }

  showNameAndPoints() {
    if (this.points.length) {
      return `Name: ${this.name}. Points: ${this.points}`;
    }
    return `Name: ${this.name}. Points: No points yet.`;
  }

  changeEmail(newEmail) {
    this.email = newEmail;
  }
}

User.arrayOfUsers = [];

const user1 = new User("John", "john@yahoo.com");
user1.addPoint(20);
user1.addPoint(30);
const user2 = new User("Jane", "jane@yahoo.com");
user2.addPoint(15);
user2.addPoint(20);
const user3 = new User("Mike", "mike@yahoo.com");

const listUsers = document.getElementById("listUsers");

User.arrayOfUsers.forEach(function (user) {
  listUsers.innerHTML += `${user.showNameAndPoints()}<br>`;
});


const changedEmailMsg = document.createElement('p');
document.body.appendChild(changedEmailMsg)

document.getElementById("changeEmailForm").addEventListener("submit", function (event) {
  event.preventDefault();

  let newEmail = document.getElementById("newEmailInput").value;

  if (!newEmail) {
      changedEmailMsg.textContent = "Please enter an email address.";
      return;
  }

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(newEmail)) {
      changedEmailMsg.textContent = "Please enter a valid email address.";
      return;
  }

  user2.changeEmail(newEmail);
  changedEmailMsg.textContent = `Jane's new email address is: ${newEmail}`;
});


document.getElementById("printWinnerBtn").addEventListener("click", function () {
  let highestTotalPoints = 0;
  let winner;

  for (let i = 0; i < User.arrayOfUsers.length; i++) {
      let totalPoints = User.arrayOfUsers[i].points.reduce((a, b) => a + b, 0);
      if (totalPoints > highestTotalPoints) {
          highestTotalPoints = totalPoints;
          winner = User.arrayOfUsers[i];
      }
  }

  const winnerMsg = document.createElement('p');
  if (winner) {
    winnerMsg.textContent = `Winner is ${winner.name}, total points: ${highestTotalPoints}`;
    document.body.appendChild(winnerMsg)
  } 
});
